import PouchDB from "pouchdb";
import yargs from "yargs";
import randomMac from "random-mac"

const db = new PouchDB("mydb");

yargs.command({
    command: 'list',
    describe: 'List of Controllers',
    handler(argv) {
        let details;
        db.allDocs({include_docs: true, descending: true}, (err, doc) => {
            details = doc.rows.map(e => {
                return {'MAC Address': `${e.doc.mac_address}`, 'Serial Number': `${e.doc.serial_number}`}
            });
            console.table(details)
        }).catch((err) => {
            console.error(err);
        });
    }
});
yargs.command(
    {
        command: 'add',
        describe: 'Add Controller',
        builder: {
            macAddress: {
                demandOption: true,
                type: 'string'
            },
            serialNumber: {
                demandOption: true,
                type: 'string'
            }
        },
        handler(argv) {
            db.put({
                _id: argv.macAddress,
                mac_address: argv.macAddress,
                serial_number: argv.serialNumber
            }).then((res) => {
                console.log("Controller inserted successfully");
            }).catch((err) => {
                console.error(err);
            });
        }
    });
yargs.command(
    {
        command: 'remove',
        describe: 'Remove a Controller',
        builder: {
            macAddress: {
                demandOption: true,
                type: 'string'
            }
        },
        handler(argv) {
            db.get(argv.macAddress).then((doc) => {
                return db.remove(doc);
            }).then((res) => {
                console.log("Controller removed successfully");
            }).catch((err) => {
                console.error(err);
            });
        }
    }).help();

yargs.parse();